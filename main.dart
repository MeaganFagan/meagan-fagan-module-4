import 'package:flutter/material.dart';
import 'login_page.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'user_profile.dart';
import 'home_page.dart';
import 'feature_screenone.dart';
import 'feature_screentwo.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //home: login_page(),
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        accentColor: Colors.deepPurpleAccent,
        scaffoldBackgroundColor: Colors.grey,
      ),
      home: AnimatedSplashScreen(
        splash: Icon(Icons.attractions_sharp,size: 200,),
        nextScreen: login_page(),
        splashTransition: SplashTransition.rotationTransition,
        duration: 3000,
        backgroundColor: Colors.purple,
      )

    );
  }
}
