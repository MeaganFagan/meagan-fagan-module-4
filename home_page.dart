
import 'package:flutter/material.dart';
import 'package:flutter_application_1/feature_screenone.dart';
import 'package:flutter_application_1/feature_screentwo.dart';
import 'package:flutter_application_1/user_profile.dart';
import 'login_page.dart';
import 'user_profile.dart';
import 'home_page.dart';
import 'feature_screenone.dart';
import 'feature_screentwo.dart';

class home_page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.purple,
        title: Text('home page'),
      ),
      body: Center(                       //have two buttons here for each feature screen
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              color: Colors.blue,
              child: Text('Go To f1'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return feature_screenone();
                    },
                  ),
                );
                //Navigate to Screen 1
              },
            ),
            RaisedButton(
              color: Colors.blue,
              child: Text('Go To f2'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return feature_screentwo();
                    },
                  ),
                );
                //Navigate to Screen 2
              },
            ),
            /*RaisedButton(
              color: Colors.blue,
              child: Text('Go To user profile'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return user_profile();
                    },
                  ),
                );
                //Navigate to Screen 3
              },
            ), */
          ],
        ),


      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return user_profile();
                    },
                  ),
                );
        },
        child: Icon(
          Icons.people,
          color: Colors.blueAccent,
        ),
        backgroundColor: Colors.white70,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniStartDocked,
    );
  }
}