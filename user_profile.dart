import 'package:flutter/material.dart';
import 'package:flutter_application_1/home_page.dart';
import 'login_page.dart';
import 'user_profile.dart';
import 'home_page.dart';
import 'feature_screenone.dart';
import 'feature_screentwo.dart';

class user_profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.blue,
        title: Text('User profile'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            //color: Colors.lightGreen,
            height: MediaQuery.of(context).size.height * 0.3,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 50,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Meagan Fagan"),
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[Text("Posts"),Text("5")],
                  ),
                  Column(
                    children: <Widget>[Text("Followers"),Text("0")],
                  ),
                  Column(
                    children: <Widget>[Text("Following"),Text("3")],
                  ),
                ],

              ),
            ),

          ),
          Card(
            child: Container(
              child: Column(
                children: <Widget>[
                  Text("User Information"),
                  Divider(),
                  ListTile(
                    title: Text("Location"),
                    subtitle: Text("Cape Town, South Africa"),
                    leading: Icon(Icons.location_on),
                  ),
                  ListTile(
                    title: Text("Email"),
                    subtitle: Text("abc@gmail.com"),
                    leading: Icon(Icons.email),
                  ),
                  ListTile(
                    title: Text("Phone"),
                    subtitle: Text("12345789"),
                    leading: Icon(Icons.phone),
                  ),
                  ListTile(
                    title: Text("About Me:"),
                    subtitle: Text("I am a final year university student"),
                    leading: Icon(Icons.info),
                  ),
                ],
              ),
            ),
          ),
        ],
        /*child: RaisedButton(
          color: Colors.blue,
          child: Text('Go Back To home page'),
          onPressed: () {
            Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return home_page();
                    },
                  ),
                );
          },
        ), */
      ),
    );
  }
}

